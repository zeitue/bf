#include <interpreter.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <version.h>

#define DEFAULT_CELLS 30000
void help(char *appname) {
  printf(
      "Usage: %s [-hv] [-c value] [FILE]\n"
      "-c, --cells=value  Specify number of cells value defaults to [%d]\n"
      "-h, --help         Display this help information and exit\n"
      "-v, --version      Output version information and exit)\n",
      appname, DEFAULT_CELLS);
}

void version() { printf("Version: %s\n", VERSION); }

void run(char *filename, uint32_t cells) {
  uint32_t cell_count = DEFAULT_CELLS;
  if (cells > cell_count) {
    cell_count = cells;
  }
  struct interpreter_t interp = {};
  interpreter_init(&interp, filename, cell_count);
  interpreter_run(&interp);
  interpreter_delete(&interp);
}

int main(int argc, char **argv) {
  uint32_t cell_count = 0;
  uint32_t next = 2;
  if (argc > 1) {
    switch (argv[1][0]) {
      case '-':
        switch (argv[1][1]) {
          case '-':
            if (strcmp("help", argv[1] + 2) == 0) {
              help(argv[0]);
              exit(0);
            }
            if (strcmp("version", argv[1] + 2) == 0) {
              version();
              exit(0);
            }

            if (strcmp("cells", argv[1] + 2) == 0 && argc > 2) {
              cell_count = strtol(argv[2], NULL, 10);
              next++;
              break;
            }

            if (strncmp(argv[1], "--cells=", 8) == 0) {
              cell_count = strtol(argv[1] + 8, NULL, 10);
              break;
            }
            break;
          case 'c':
            if (strlen(argv[1]) > 3) {
              cell_count = strtol(argv[1] + 3, NULL, 10);
            } else if (argc > 2) {
              cell_count = strtol(argv[2], NULL, 10);
              next++;
            }
            break;
          case 'v':
            version();
            exit(0);
          case 'h':
            help(argv[0]);
            exit(0);
          default:
            help(argv[0]);
            exit(0);
        }
        break;
      default:
        run(argv[1], cell_count);
    }
    if (argc > next) {
      run(argv[next], cell_count);
    }
  } else {
    help(argv[0]);
  }

  return 0;
}
