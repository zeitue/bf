#include <errno.h>
#include <fcntl.h>
#include <interpreter.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void interpreter_build_jump_table(struct interpreter_t* interp) {
  uint32_t* stack = calloc(interp->code_size, sizeof(uint32_t));
  uint32_t top = 0;
  if (stack == NULL) {
    printf("Error: allocating memory\n");
    exit(ENOMEM);
  }

  for (uint32_t i = 0; i < interp->code_size; i++) {
    switch (interp->code[i].opcode) {
      case '[':
        stack[top++] = i;
        break;
      case ']':
        interp->code[i].jump = stack[top - 1];
        interp->code[stack[top - 1]].jump = i;
        stack[top - 1] = 0;
        top--;
        break;
    }
  }
  free(stack);
}

void interpreter_init(struct interpreter_t* interp, char* filename,
                      uint32_t cell_count) {
  interp->cp = 0;
  interp->pc = 0;

  if (access(filename, F_OK) != 0) {
    printf("Error: no such file\n");
    exit(ENOENT);
  }

  FILE* fp = fopen(filename, "r");

  if (fp == NULL) {
    printf("Error: could not open file\n");
    exit(EIO);
  }

  fseek(fp, 0L, SEEK_END);
  size_t size = ftell(fp);
  interp->code_size = size;
  rewind(fp);

  char* code = malloc(size);
  if (code == NULL) {
    printf("Error: allocating memory\n");
    exit(ENOMEM);
  }
  if (fread(code, size, 1, fp) != 1) {
    printf("Error: file not fully read\n");
  }
  fclose(fp);

  interp->code = calloc(size, sizeof(struct instruction_t));
  if (interp->code == NULL) {
    printf("Error: allocating memory\n");
    exit(ENOMEM);
  }

  for (size_t s = 0; s < size; s++) {
    interp->code[s].opcode = code[s];
  }
  free(code);

  interp->cells = calloc(cell_count, sizeof(uint8_t));
  if (interp->cells == NULL) {
    printf("Error: allocating memory\n");
    exit(ENOMEM);
  }
  interpreter_build_jump_table(interp);
}

void interpreter_delete(struct interpreter_t* interp) {
  if (interp == NULL) {
    return;
  }

  if (interp->cells != NULL) {
    free(interp->cells);
  }

  if (interp->code != NULL) {
    free(interp->code);
  }
}

void interpreter_run(struct interpreter_t* interp) {
  for (; interp->pc < interp->code_size; interp->pc++) {
    switch (interp->code[interp->pc].opcode) {
      case '<':
        interp->cp--;
        break;
      case '>':
        interp->cp++;
        break;
      case '+':
        interp->cells[interp->cp]++;
        break;
      case '-':
        interp->cells[interp->cp]--;
        break;
      case ',':
        interp->cells[interp->cp] = getchar();
        break;
      case '.':
        putchar((char)interp->cells[interp->cp]);
        break;
      case '[':
        if (interp->cells[interp->cp] == '\0') {
          interp->pc = interp->code[interp->pc].jump;
        }
        break;
      case ']':
        if (interp->cells[interp->cp] != '\0') {
          interp->pc = interp->code[interp->pc].jump;
        }
        break;
      default:
        break;
    }
  }
}
