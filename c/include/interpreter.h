#ifndef _INTERPRETER_H_
#define _INTERPRETER_H_

#include <stdint.h>

struct instruction_t {
  uint32_t jump;
  uint8_t opcode;
  uint8_t pad[3];
};

struct interpreter_t {
  uint8_t* cells;
  struct instruction_t* code;
  uint32_t pc;
  uint32_t cp;
  uint32_t code_size;
};

void interpreter_init(struct interpreter_t* interp, char* filename,
                      uint32_t cell_count);
void interpreter_delete(struct interpreter_t* interp);
void interpreter_run(struct interpreter_t* interp);

#endif /* _INTERPRETER_H_ */