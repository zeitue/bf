/*
 * main.cpp
 *
 *  Created on: 2014-10-18
 *      Author: zeitue
 */

#include <string>

#include "Interpreter.hpp"
#include "Utilities.hpp"

int main(int argc, char **argv) {
  uint32_t cell_count = 0;
  int next = 2;
  if (argc > 1) {
    std::string arg1(argv[1]);
    switch (arg1[0]) {
      case '-':
        switch (arg1[1]) {
          case '-':
            if ("--help" == arg1) {
              bf::help(argv[0]);
              exit(0);
            }
            if ("--version" == arg1) {
              bf::version();
              exit(0);
            }

            if ("--cells" == arg1 && argc > 2) {
              cell_count = strtol(argv[2], NULL, 10);
              next++;
              break;
            }
            if (arg1.substr(2, 6) == "cells=") {
              cell_count = strtol(argv[1] + 8, NULL, 10);
              break;
            }
            break;
          case 'c':
            if (arg1.length() > 3) {
              cell_count = strtol(argv[1] + 3, NULL, 10);
            } else if (argc > 2) {
              cell_count = strtol(argv[2], NULL, 10);
              next++;
            }
            break;
          case 'v':
            bf::version();
            exit(0);
          case 'h':
            bf::help(argv[0]);
            exit(0);
          default:
            bf::help(argv[0]);
            exit(0);
        }
        break;
      default:
        bf::interprete(argv[1], cell_count);
    }
    if (argc > next) {
      bf::interprete(argv[next], cell_count);
    }
  } else {
    bf::help(argv[0]);
  }

  return 0;
}
