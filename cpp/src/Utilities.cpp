/*
 * Utilities.cpp
 *
 *  Created on: 2014-10-18
 *      Author: zeitue
 */
#include <Interpreter.hpp>
#include <Utilities.hpp>
#include <version.hpp>

namespace bf {

void help(std::string name) {
  std::cout << "Usage: " << name << " [-h] [options] filename.bf";
  std::cout << std::endl << std::endl << "Options:" << std::endl;
  std::cout << "\t-c<number>\tSpecify number of cells [30000]" << std::endl;
  std::cout << "\t-v\t\tPrint version and exit" << std::endl;
  std::cout << "\t-h\t\tDisplay this help and exit" << std::endl;
}

void version() {
  std::cout << "bf: a Brainf*ck interpreter" << std::endl
            << "Version: " << VERSION << std::endl;
}

void interprete(std::string name, int cell_size) {
  int cell_count = DEFAULT_CELLS;
  if (cell_size > DEFAULT_CELLS) {
    cell_count = cell_size;
  }
  Interpreter i(name, cell_count);
  i.eval();
}

}  // namespace bf
