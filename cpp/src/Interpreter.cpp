/*
 * Interpreter.cpp
 *
 *  Created on: 2014-10-18
 *      Author: zeitue
 */

#include "Interpreter.hpp"
namespace bf {

Interpreter::Interpreter(std::string filename, int cell_size) {
  cp = 0;
  ip = 0;
  std::ifstream file(filename.c_str());
  if (!file) {
    if (filename == "")
      std::cout << "No file to open" << std::endl;
    else
      std::cout << "Failed to open \"" << filename << "\"" << std::endl;
    exit(0);
  } else {
    slurp(file);
    cells = new char[cell_size]();
  }
  file.close();
  this->optimize();
}

Interpreter::~Interpreter() {
  delete[] cells;
  delete code;
}

void Interpreter::optimize() {
  std::stack<uint32_t> brackets;
  for (uint32_t i = 0; i < code->size(); i++) {
    switch ((*code)[i].opcode) {
      case '[':
        brackets.push(i);
        break;
      case ']':
        uint32_t target = brackets.top();
        (*code)[i].jump = target;
        (*code)[target].jump = i;
        brackets.pop();
        break;
    }
  }
}

void Interpreter::slurp(std::ifstream& in) {
  std::stringstream sstr;
  sstr << in.rdbuf();
  std::string buf = sstr.str();
  code = new std::vector<Instruction>(buf.length());
  for (std::string::size_type i = 0; i < buf.size(); ++i) {
    (*code)[i].opcode = buf[i];
  }
}

void Interpreter::eval() {
  for (ip = 0; ip < code->size(); ip++) {
    switch ((*code)[ip].opcode) {
      case '<':
        --cp;
        break;
      case '>':
        ++cp;
        break;
      case '+':
        ++cells[cp];
        break;
      case '-':
        --cells[cp];
        break;
      case ',':
        cells[cp] = getchar();
        break;
      case '.':
        std::cout << cells[cp];
        break;
      case '[':
        if (cells[cp] == '\0') {
          ip = (*code)[ip].jump;
        }
        break;
      case ']':
        if (cells[cp] != '\0') {
          ip = (*code)[ip].jump;
        }
        break;
    }
  }
}

}  // namespace bf
