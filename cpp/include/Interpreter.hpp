/*
 * Interpreter.hpp
 *
 *  Created on: 2014-10-18
 *      Author: zeitue
 */

#ifndef INTERPRETER_HPP_
#define INTERPRETER_HPP_

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <vector>
namespace bf {
const int DEFAULT_CELLS = 30000;

class Instruction {
 public:
  char opcode;
  int jump;
};

class Interpreter {
 private:
  std::vector<Instruction>* code;
  char* cells;
  uint32_t cp;
  uint32_t ip;
  void slurp(std::ifstream&);

 public:
  Interpreter(std::string = "", int = DEFAULT_CELLS);
  ~Interpreter();
  void optimize();
  void eval();
};

}  // namespace bf
#endif /* INTERPRETER_HPP_ */
