use std::fs;
use std::io::{stdin, Read};
use std::process;
use std::vec::Vec;

const MIN_CELLS: usize = 10000;
const MAX_CELLS: usize = 1000000000;

#[derive(Clone)]
struct Instruction {
    opcode: char,
    jump: usize,
}

pub struct Interpreter {
    cells: Vec<u8>,
    code: Vec<Instruction>,
    ip: usize,
    cp: usize,
}

impl Interpreter {
    pub fn new(cell_count: usize, filename: String) -> Self {
        let code = match fs::read_to_string(&filename) {
            Ok(result) => result
                .chars()
                .map(|opcode| Instruction {
                    opcode: opcode,
                    jump: 0,
                })
                .collect(),
            Err(_) => {
                println!("Error: file {} not found or invalid", filename);
                process::exit(0xff);
            }
        };

        let cells = if (MIN_CELLS..=MAX_CELLS).contains(&cell_count) {
            vec![0; cell_count]
        } else {
            println!("Error: invalid cell count {}", cell_count);
            process::exit(0xff);
        };

        let mut interp = Self {
            cells,
            code,
            cp: 0,
            ip: 0,
        };
        interp.build_jump_table();
        interp
    }

    pub fn run(&mut self) {
        while self.ip < self.code.len() {
            match self.code[self.ip].opcode {
                '>' => self.cp += 1,
                '<' => self.cp -= 1,
                '+' => self.cells[self.cp] = self.cells[self.cp].wrapping_add(1),
                '-' => self.cells[self.cp] = self.cells[self.cp].wrapping_sub(1),
                '.' => print!("{}", self.cells[self.cp] as char),
                ',' => {
                    let input: u8 = stdin()
                        .bytes()
                        .next()
                        .and_then(|result| result.ok())
                        .map(|byte| byte as u8)
                        .unwrap();
                    self.cells[self.cp] = input;
                }
                '[' => {
                    if self.cells[self.cp] == 0 {
                        self.ip = self.code[self.ip].jump;
                    }
                }
                ']' => {
                    if self.cells[self.cp] != 0 {
                        self.ip = self.code[self.ip].jump;
                    }
                }
                _ => {}
            }
            self.ip += 1;
        }
    }

    pub fn build_jump_table(&mut self) {
        let mut stack: Vec<usize> = Vec::new();

        for (i, c) in self.code.clone().iter().enumerate() {
            match c.opcode {
                '[' => {
                    stack.push(i);
                }
                ']' => {
                    if let Some(start) = stack.pop() {
                        self.code[start].jump = i;
                        self.code[i].jump = start;
                    }
                }
                _ => {}
            }
        }
    }
}
