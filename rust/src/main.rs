use crate::bf::Interpreter;
use std::env;
use std::process;

mod bf;

fn help(app_name: &str) {
    println!("Usage: {} [OPTION] [FILE]", app_name);
    println!("Options:");
    println!("-c N\t\tSpecify number of cells [N] defaults to 30000");
    println!("-v\t\tOutput version information and exit");
    println!("-h\t\tDisplay this help information and exit");
}

fn version(app_name: &str) {
    println!("{}", app_name);
    println!("Version: {}", env!("VERGEN_GIT_SEMVER"));
}

fn main() {
    let app_name = env::args().next().unwrap_or_else(|| "bf-rs".to_string());

    let option = match env::args().nth(1) {
        Some(option) => option,
        None => {
            help(&app_name);
            process::exit(0xff)
        }
    };

    match option.as_ref() {
        "-c" => match env::args().nth(2) {
            Some(count) => {
                let cell_count = count.parse::<usize>().unwrap_or(0);
                match env::args().nth(3) {
                    Some(filename) => {
                        let mut bf = Interpreter::new(cell_count, filename);
                        bf.run();
                    }
                    None => {
                        println!("Error: no file given");
                        process::exit(0xff);
                    }
                }
            }
            None => {
                println!("Error: no cell count given");
                process::exit(0xff);
            }
        },
        "-v" => version(&app_name),
        "-h" => help(&app_name),
        _ => help(&app_name),
    }
}
