# bf-rs

bf-rs is a simple brainf*ck interpreter written in Rust.

## Buidling

To build bf-rs simply cd to its directory and type `cargo build` or `cargo build --release`.

## License

Licensed under the GNU General Public License.

## Contributing

All contributions are welcome.
