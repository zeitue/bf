package main

import (
	"errors"
	"fmt"
	"os"

	getopt "github.com/pborman/getopt/v2" // version 2
	interpreter "github.com/zeitue/bfgo/internal/bfgo"
)

var (
	cells   int32 = 30000
	help    bool  = false
	version bool  = false
)

func main() {

	getopt.SetParameters("[FILE]")
	getopt.FlagLong(&cells, "cells", 'c', "Specify number of cells value defaults to")
	getopt.FlagLong(&help, "help", 'h', "Display this help information and exit")
	getopt.FlagLong(&version, "version", 'v', "Output version information and exit")
	getopt.Parse()

	if version {
		fmt.Println("Version: ")
		os.Exit(0)
	}

	if help || getopt.NArgs() < 1 {
		getopt.Usage()
		os.Exit(0)
	}

	filename := (getopt.Args()[0])
	if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
		fmt.Printf("Error: file '%v' does exist\n", filename)
		os.Exit(1)
	}

	interp := interpreter.NewInterpeter(filename, cells)
	interp.Execute()
}
