package bfgo

import (
	"bufio"
	"fmt"
	"os"
)

type Instruction struct {
	opcode rune
	jump   uint32
}

type Interpreter struct {
	Cells              []byte
	Code               []Instruction
	InstructionPointer uint32
	CellPointer        uint32
}

func NewInterpeter(filename string, cellCount int32) Interpreter {

	b, err := os.ReadFile(filename)
	if err != nil {
		fmt.Print(err)
	}

	i := Interpreter{
		Cells:              make([]byte, cellCount),
		Code:               make([]Instruction, len(b)),
		InstructionPointer: 0,
		CellPointer:        0,
	}
	for idx, instr := range b {
		i.Code[idx].opcode = rune(instr)
	}
	i.BuildJumpTable()
	return i
}

func (i Interpreter) Execute() {
	for ; i.InstructionPointer < uint32(len(i.Code)); i.InstructionPointer++ {
		switch cmd := i.Code[i.InstructionPointer].opcode; cmd {
		case '>':
			i.CellPointer++
		case '<':
			i.CellPointer--
		case '+':
			i.Cells[i.CellPointer]++
		case '-':
			i.Cells[i.CellPointer]--
		case '.':
			fmt.Printf("%c", rune(i.Cells[i.CellPointer]))
		case ',':
			in := bufio.NewReader(os.Stdin)
			r, _, _ := in.ReadRune()
			i.Cells[i.CellPointer] = byte(r)
		case '[':
			if i.Cells[i.CellPointer] == 0 {
				i.InstructionPointer = i.Code[i.InstructionPointer].jump
			}
		case ']':
			if i.Cells[i.CellPointer] != 0 {
				i.InstructionPointer = i.Code[i.InstructionPointer].jump
			}
		default:
		}
	}
}

func (i *Interpreter) BuildJumpTable() {
	stk := make([]uint32, 0)
	for idx, code := range i.Code {
		switch code.opcode {
		case '[':
			stk = append(stk, uint32(idx))
		case ']':
			{
				l := len(stk)
				trg := stk[l-1]
				stk = stk[:l-1]
				i.Code[idx].jump = trg
				i.Code[trg].jump = uint32(idx)
			}
		default:
		}
	}
}
